import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'


const animalFuncns = {
  walk: () => <p>Walking</p>,
  sleep: () => <p>Sleeping</p>,
};

const dogFuncns = {
  bark: () => <p>Barking</p>,
};

const lionFuncns = {
  roar: () => <p>Roaring</p>,
};

class Dog {
  constructor(name){
    this.name = name;
  }
}

class Lion {
  constructor(name){
    this.name = name;
  }
};

Object.assign(dogFuncns, animalFuncns);  // Mixins
Object.assign(Dog.prototype, dogFuncns);  // Mixins

Object.assign(lionFuncns, animalFuncns);  // Mixins
Object.assign(Lion.prototype, lionFuncns);  // Mixins

const tommy = new Dog("Tommy");
const sheru = new Lion("sheru");

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <h2>Tommy::{tommy.bark()}</h2> <br/>
      <h2>{`Sheru::`}{sheru.roar()}</h2>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
