import React from "react";
import EmojiPicker from "emoji-picker-react";

const StaticImport = () => {
  return (
    <div>
      <EmojiPicker />
    </div>
  );
};

export default StaticImport;
