import React, { Suspense, lazy } from 'react';


const EmojiPicker = lazy(() => import("emoji-picker-react"));


const DynamicImport = () => {
  return (
    <div>
        <Suspense fallback={<h3>LazyLoader Emoji Component is Loading...</h3>}>
            <EmojiPicker />
        </Suspense>
    </div>
  )
}

export default DynamicImport;