import { animalFuncns } from "./utils/animal";

class Lion {
  constructor(name) {
    this.name = name;
  }
}

const lionFuncns = {
  roar: () => <p>Roaring</p>,
};

Object.assign(lionFuncns, animalFuncns); // Mixins
Object.assign(Lion.prototype, lionFuncns); // Mixins

export default Lion;
