import { animalFuncns } from "./utils/animal";

class Dog {
  constructor(name) {
    this.name = name;
  }
}

const dogFuncns = {
  bark: () => <p>Barking</p>,
};

Object.assign(dogFuncns, animalFuncns); // Mixins

Object.assign(Dog.prototype, dogFuncns); // Mixins

export default Dog;
