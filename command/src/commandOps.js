import Command from "./command";
function CreateUserCommand(name) {
  return new Command((users) => users.push(name));
}

function DeleteUserCommand(index) {
  return new Command((users) => users.splice(index, 1));
}

export const Operations = { CreateUserCommand, DeleteUserCommand };
