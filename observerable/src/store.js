class Store {
    count = 1;
    subscriberMethods = [];

    changeCount = (newValue) => {
        this.count = newValue;
        this.subscriberMethods.forEach((each) => each(newValue));
    };

    increment = () => {
        this.changeCount(this.count + 1);
    }

    subscribe = (callBackFn) => {
        this.subscriberMethods.push(callBackFn);
    };
};

export const store = new Store();