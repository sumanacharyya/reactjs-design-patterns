import React, { useEffect, useState } from "react";
import { store } from "./store";

export const useStore = (defaultValue) => {

    const [value, setValue] = useState(defaultValue);

    useEffect(() => {
        store.subscribe(setValue);
    }, []);
    return value;
};