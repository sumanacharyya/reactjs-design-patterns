import { useState } from 'react'
import reactLogo from './assets/react.svg'
import {store} from './store';
import './App.css'
import { useStore } from './useStore';

const Text = () => {
  const storeCount = useStore(1);
  return (
    <h2>{storeCount}</h2>
  )
}
const Text2 = () => {
  const storeCount = useStore(0);
  return (
    <h2>{storeCount}</h2>
  )
}

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <Text />
      <Text2 />
      <button onClick={(e) => store.increment()}>Increment the custom Count</button>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
