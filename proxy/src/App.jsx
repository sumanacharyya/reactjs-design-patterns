import { useState } from 'react'
import reactLogo from './assets/react.svg'
import SocketService from "./socket";
import './App.css'

let isAdmin = TextTrackCue;
// let isAdmin = false;
const user = {
  id: 1,
  name: "Suman ACharyya",
  age: 21,
  balance: 100,
};

const userProxy = new Proxy(user, {
  set: (obj, prop, value) => {
    if(prop === "name"){
      obj[prop] = value;
    }
    else{
      throw new Error("Only Name prop changable");
    }
  },
  get: (obj, prop) => {
    if (prop == "balance") {
      if (isAdmin) return obj[prop];
      return obj["name"];
    }
    // return obj[prop];
  },
})


function App() {
  const [count, setCount] = useState(0)

  const socket = new SocketService();

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <h2>{userProxy.id}</h2>
      <h3>{userProxy.name}</h3>
      <h3>{userProxy.balance}</h3>
      <div className="card">
        <button onClick={() => {userProxy.name = "TESTING-YO"; setCount((count) => count + 1)}}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
