import { useState } from 'react'
import reactLogo from './assets/react.svg'
import SocketService from "./socket";
import './App.css'


const userFactory = ({ fname, lname, age }) => ({
  firstName: fname,
  lastName: lname,
  age,
  fullName() {
    return `${fname} ${lname}`;
  },
  isEligibleToVote() {
    return `${age >= 18}`;
  },
});

const piyush = userFactory({ fname: "Suman", lname: "Acharyya", age: 12 });

const mehul = userFactory({ fname: "ABC", lname: "XYZ", age: 18 });

const jhon = userFactory({ fname: "Foo", lname: "Bar", age: 20 });

const users = [piyush, mehul, jhon];


function App() {
  const [count, setCount] = useState(0)

  const socket = new SocketService();

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      {users.map((user, i) => (
          <li key={i}>
            {user.fullName()} - Can they vote {user.isEligibleToVote()}
          </li>
        ))}
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App

