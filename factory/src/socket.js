let instance = null;

class SocketService {
    constructor() {
    //   console.log("IO conection built");
    if (!instance) {
      console.log("IO conection built");
      instance = this;
    }
    return instance;
  }

  sendData() {
    console.log("Send Data on IO");
  }
}

export default SocketService;