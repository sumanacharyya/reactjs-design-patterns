import React, { createContext, useContext } from "react";
import { useState } from "react";

export const ThemeContext = createContext(null);

export const useTheme = () => {  // Custom Hook Theme Toggler 
    return useContext(ThemeContext);
}

export const ThemeProvider = ({ children }) => {
  const [mode, setMode] = useState("light");

  const ToggleMode = () => {
    setMode((prev) => (prev === "light" ? "dark" : "light"));
  };

  return (
    <ThemeContext.Provider value={[mode, ToggleMode]}>
      {children}
    </ThemeContext.Provider>
  );
};
