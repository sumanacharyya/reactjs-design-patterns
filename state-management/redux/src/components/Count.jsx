import React from "react";
import { useSelector } from "react-redux";

const Count = () => {
  const state = useSelector((state) => state);
  return <h3>{state}</h3>;
};

export default Count;
