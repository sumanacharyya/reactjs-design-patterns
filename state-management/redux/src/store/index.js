import { createStore } from "redux";

function reducerFunc(state = 1, action) {
  switch (action.type) {
    case "increment":
      return state + 1;

    case "decreemnt":
      return state - 1;

    default:
      return state;
  }
}

export const store = createStore(reducerFunc);
