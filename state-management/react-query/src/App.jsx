import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { useQuery, useQueryClient } from "@tanstack/react-query";

const getAllUsers = async () => {
  return await (await fetch("https://dummyjson.com/users")).json();
};

function App() {
  const [count, setCount] = useState(0);

  const { data, status } = useQuery(["users"], async () => getAllUsers());

  const queryClient = useQueryClient();

  if (status === "loading") return <h1>Loading...</h1>;
  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <h2>User List</h2>
      {
        data.users.length > 0 &&
        data.users.map((user, index) => (
          <p key={index}>
            {user.firstName} {user.lastName} with age {user.age}.
          </p>
        ))
      }
      <button onClick={() => queryClient.invalidateQueries("users")}>Invalidate and Refresh Users</button>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  );
}

export default App;
