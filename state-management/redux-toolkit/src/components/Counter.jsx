import React from 'react';
import { useSelector } from 'react-redux';
import { getCount } from '../redux/slices/counterSlice';

const Counter = () => {
    const state = useSelector(getCount);
  return (
    <h2>{state}</h2>
  )
};

export default Counter;