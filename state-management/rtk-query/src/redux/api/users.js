import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const userApi = createApi({
  reducerPath: "userApi",
  baseQuery: fetchBaseQuery({ baseUrl: "https://dummyjson.com/" }),
  endpoints: (builder) => ({
    getAllUsers: builder.query({
        query: () => {
            return `/users`
        },
    }),
  }),
});

export const {useGetAllUsersQuery}  = userApi;
