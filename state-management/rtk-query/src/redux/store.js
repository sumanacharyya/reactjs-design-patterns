import { userApi } from "./api/users";

import { configureStore } from "@reduxjs/toolkit";

export const store  = configureStore({
    reducer: {
        [userApi.reducerPath]: userApi.reducer,
    },
    middleware: (getDeafultMiddleware) => {
        return getDeafultMiddleware().concat(userApi.middleware);
    },
});
