import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { useCountStore } from "./store/counter";

function App() {
  const [count, setCount] = useState(0);
  const {value, increment, decrement} = useCountStore();
  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <h2>Zustand Counter</h2>
      
      <button onClick={decrement}>-</button>
      {value}
      <button onClick={increment}>+</button>

      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  );
}

export default App;
