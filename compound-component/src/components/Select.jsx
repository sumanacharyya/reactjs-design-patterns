import React, { createContext } from "react";
import { useState } from "react";
import { useContext } from "react";

export const SeelctContext = createContext(null);

export const useSelect = () => {
  const state =  useContext(SeelctContext);
  if(!state) throw new Error("Render Option should inside of Select");
  return state;
};

const Select = ({val, children }) => {
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState("");

  return (
    <div className="select-container">
      <SeelctContext.Provider value={[value, setValue]}>
        <button onClick={() => setOpen((prev) => !prev)}>
          Toggle Option Selected for {val} - {value}
        </button>
        {open && children}
      </SeelctContext.Provider>
    </div>
  );
};

export default Select;
