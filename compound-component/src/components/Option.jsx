import React from 'react'
import { useSelect } from './select';

const Option = ({value, text}) => {
const [activeValue, setActiveValue ] = useSelect();

  return (
    <div onClick={() => setActiveValue(value)} className={activeValue === value ? "selected-active" : ""}>
        {text}
    </div>
  )
}

export default Option;