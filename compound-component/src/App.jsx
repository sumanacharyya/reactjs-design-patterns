import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Select from './components/select'
import Option from './components/option'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <Select val="Languages/Libs/Frameworks">
        <Option value={"JS"} text="JaavScript" />
        <Option value={"NodeJS"} text="Node.JS" />
        <Option value={"ReactJS"} text="React.JS" />
        <Option value={"NEXTJS"} text="NEXT.JS" />
        <Option value={"EXPRESSJS"} text="EXPRESS.JS" />
        <Option value={"NESTJS"} text="NEST.JS" />
      </Select>
      <Select val="DataBase" >
        <Option value={"MongoDB"} text="MongoDB" />
        <Option value={"PostgreSQL"} text="PostgreSQL" />
        <Option value={"CloudFirestore"} text="Cloud Firestore" />
        <Option value={"DynamoDB"} text="DynamoDB" />
        <Option value={"MySQL"} text="MySQL" />
      </Select>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
